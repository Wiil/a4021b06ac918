# syntax=docker/dockerfile:1
FROM golang:1.18 AS build
MAINTAINER Willy Duville <willy.duville@gmail.com>

WORKDIR /

# Download Go modules
COPY go.mod ./
COPY go.sum ./
RUN go mod download

# Copy source files
COPY *.go ./
COPY api/*.go ./api/
COPY fizzbuzz/*.go ./fizzbuzz/

RUN go build -o /go/bin/fizzbuzzapi

#
# "Distroless" Container Images (See https://github.com/GoogleContainerTools/distroless)
#
FROM gcr.io/distroless/base-debian11

WORKDIR /

COPY --from=build /go/bin/fizzbuzzapi /

EXPOSE 8080

USER nonroot:nonroot

ENTRYPOINT ["/fizzbuzzapi"]
