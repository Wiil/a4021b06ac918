package api

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"golang.org/x/exp/slices"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func init() {
	gin.SetMode(gin.TestMode)
}

func performRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}

func TestFizzBuzzOriginal(t *testing.T) {
	const limit = 16
	expectedVal := strings.Split("1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16", ",")

	statsMap := make(map[string]int)
	router := InitServer(statsMap)

	w := performRequest(router, "GET", "/fizzbuzz?int1=3&int2=5&limit=16&str1=fizz&str2=buzz")
	if w.Code != 200 {
		t.Fatalf("w.Code != 200")
	}
	var msg []string
	err := json.Unmarshal([]byte(w.Body.String()), &msg)
	if err != nil {
		t.Fatalf("Illegal Json : %s", err.Error())
	}

	if !slices.Equal(msg[:limit], expectedVal) {
		t.Fatalf("fizzbuz =\n %q, want\n %q, error", msg[:limit], expectedVal)
	}
}

func TestFizzBuzzInvalidParams(t *testing.T) {
	statsMap := make(map[string]int)
	router := InitServer(statsMap)

	w := performRequest(router, "GET", "/fizzbuzz?int1=deux&int2=5")
	if w.Code != 400 {
		t.Fatalf("w.Code == %d. Expected 200", w.Code)
	}
}

func TestFizzBuzzZeroParam(t *testing.T) {
	statsMap := make(map[string]int)
	router := InitServer(statsMap)

	w := performRequest(router, "GET", "/fizzbuzz?int1=2&int2=0")
	if w.Code != 400 {
		t.Fatalf("w.Code == %d. Expected 200", w.Code)
	}
}

func TestStatsEmpty(t *testing.T) {
	statsMap := make(map[string]int)
	router := InitServer(statsMap)

	// Initial state
	w := performRequest(router, "GET", "/stats")
	if w.Code != 200 {
		t.Fatalf("w.Code != 200")
	}
	if w.Body.String() != "{\"most_requested\":{}}" {
		t.Fatalf("w.Body.String() == %s expected {\"most_requested\":{}}", w.Body.String())
	}
}

func TestStats(t *testing.T) {
	const calls = 199
	statsMap := make(map[string]int)
	router := InitServer(statsMap)

	w := performRequest(router, "GET", "/stats")

	for i := 0; i < calls/2; i++ {
		w = performRequest(router, "GET", "/fizzbuzz?int1=5&int2=8&limit=150&str1=foo&str2=bar")
		if w.Code != 200 {
			t.Fatalf("w.Code != 200")
		}
	}
	for i := 0; i < calls; i++ {
		w = performRequest(router, "GET", "/fizzbuzz?int1=3&int2=5&limit=15&str1=fizz&str2=buzz")
		if w.Code != 200 {
			t.Fatalf("w.Code != 200")
		}
	}
	for i := 0; i < calls/3; i++ {
		w = performRequest(router, "GET", "/fizzbuzz?int1=2&int2=8&limit=150&str1=foo&str2=bar")
		if w.Code != 200 {
			t.Fatalf("w.Code != 200")
		}
	}

	w = performRequest(router, "GET", "/stats")
	js := fmt.Sprintf(`{"most_requested":{"3, 5, 15, fizz, buzz":%d}}`, calls)

	if w.Body.String() != js {
		t.Fatalf("w.Body.String() Expected\n%T %s !=\n%T %s", w.Body.String(), w.Body.String(), js, js)
	}
}
