package api

import (
	"github.com/gin-gonic/gin"
	"largus-lbc-fizzbuzzapi/fizzbuzz"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
)

type Params struct {
	int1  int
	int2  int
	limit int
	str1  string
	str2  string
}

func InitServer(statsMap map[string]int) *gin.Engine {
	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	if err := router.SetTrustedProxies(nil); err != nil {
		print(err)
	}

	router.GET("/fizzbuzz", func(c *gin.Context) {
		ComputeHandler(c, statsMap)
	})
	router.GET("/stats", func(c *gin.Context) {
		StatsHandler(c, statsMap)
	})
	return router
}

func StartServer(addr string, router *gin.Engine) {
	if addr == "" {
		addr = "localhost:8080"
	}
	if err := router.Run(addr); err != nil {
		log.Fatal(err.Error())
		return
	}
}

func ComputeHandler(c *gin.Context, statsMap map[string]int) {
	var err error
	var p = Params{
		str1: c.DefaultQuery("str1", "fizz"),
		str2: c.DefaultQuery("str2", "buzz"),
	}

	p.int1, err = strconv.Atoi(c.Query("int1"))
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "int1 param should be an non zero integer", "error": err.Error()})
		return
	} else if p.int1 == 0 {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "A non zero integer is expected for 'int1' param", "error": "int1 == 0"})
		return
	}

	p.int2, err = strconv.Atoi(c.Query("int2"))
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "int2 param should be an non zero integer", "error": err.Error()})
		return
	} else if p.int2 == 0 {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "A non zero integer is expected for 'int2' param", "error": "int2 == 0"})
		return
	}

	p.limit, err = strconv.Atoi(c.DefaultQuery("limit", "100"))
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, gin.H{"message": "limit param should be a positive integer", "error": err.Error()})
		return
	}

	statsUpdate(p, statsMap)
	result := fizzbuzz.Fizzbuzz(p.int1, p.int2, p.limit, p.str1, p.str2)

	c.IndentedJSON(http.StatusOK, strings.Split(result, ","))
}

func StatsHandler(c *gin.Context, statsMap map[string]int) {
	res := struct {
		key   string
		value int
	}{}

	// Sorting keys is necessary in Go since map order is not consistent
	// "The iteration order over maps is not specified and is not guaranteed to be the same from one iteration to the next."
	// https://go.dev/ref/spec#For_statements
	keys := make([]string, 0)
	for k := range statsMap {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, key := range keys {
		if statsMap[key] >= res.value {
			res.key = key
			res.value = statsMap[key]
		}
	}
	result := make(map[string]int)
	if res.key != "" {
		result[res.key] = statsMap[res.key]
	}
	c.JSON(http.StatusOK, gin.H{
		"most_requested": result,
		// "statsMap": statsMap,
	})
}

// Stats module
func statsUpdate(p Params, stats map[string]int) {
	key := strings.Join(
		[]string{
			strconv.Itoa(p.int1),
			strconv.Itoa(p.int2),
			strconv.Itoa(p.limit),
			p.str1,
			p.str2,
		},
		", ")
	if _, exists := stats[key]; !exists {
		stats[key] = 0
	}
	stats[key] += 1
}
