/*
FizzBuzzAPI .

The original fizz-buzz consists in writing all numbers from 1 to 100, and
just replacing all multiples of 3 by "fizz", all multiples of 5 by "buzz",
and all multiples of 15 by "fizzbuzz". The output would look like
this: "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,...".

Usage:

    GET http://localhost:8080/fizzbuzz?int1=3&int2=5&limit=100&str1=fizz&str2=buzz

Your goal is to implement a web server that will expose a REST API endpoint that:
	- Accepts five parameters : three integers int1, int2 and limit, and two strings str1 and str2.
	- Returns a list of strings with numbers from 1 to limit, where:
		- all multiples of int1 are replaced by str1
		- all multiples of int2 are replaced by str2
		- all multiples of int1 and int2 are replaced by str1str2.

Bonus question :
	- Add a statistics endpoint allowing users to know what the most frequent request has been. This endpoint should:
	- Accept no parameter
	- Return the parameters corresponding to the most used request, as well as the number of hits for this request
*/
package main

import (
	"largus-lbc-fizzbuzzapi/api"
	"log"
)

func main() {
	statsMap := make(map[string]int)
	router := api.InitServer(statsMap)

	const addr = "0.0.0.0:8080"

	log.Println("FizzBuzz API : Ready on", addr)
	api.StartServer(addr, router)
}
