# FizzBuzzAPI

## Assignment

The original fizz-buzz consists in writing all numbers from 1 to 100, and
just replacing all multiples of 3 by "fizz", all multiples of 5 by "buzz",
and all multiples of 15 by "fizzbuzz". The output would look like
this: "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16,...".

Your goal is to implement a web server that will expose a REST API endpoint that:
- Accepts five parameters : three integers int1, int2 and limit, and two strings str1 and str2.
- Returns a list of strings with numbers from 1 to limit, where:
- all multiples of int1 are replaced by str1
- all multiples of int2 are replaced by str2
- all multiples of int1 and int2 are replaced by str1str2.

Bonus question :
- Add a statistics endpoint allowing users to know what the most frequent request has been. This endpoint should:
  - Accept no parameter
  - Return the parameters corresponding to the most used request, as well as the number of hits for this request


## Comments

- A stub of an Open API spec & doc is located in `api/openapi-spec/swagger.yaml`

- A better idea for a stats module may be persisted in a DBMS (Postgres, CouchDB, SqLite...).
  However to keep the project very simple, we handled an in-memory pointer based map

- Regarding the specific case of a tie in the most frequent called queries : Sorting keys of
  a map is necessary in Go since a map's objects order is not consistent "The iteration order over maps
  is not specified and is not guaranteed to be the same from one iteration to the next."
  (See https://go.dev/ref/spec#For_statements (3.), )


### Usage

`go run .` or
 
      go build . 
      ./largus-lbc-fizzbuzz

Docker image build & run

      docker build --tag fizzbuzz .
      docker run --rm -ti -p 8080:8080 -t fizzbuzz

(Note: API will listen to port :8080)

### Tests

Run all tests : `go test --cover -v ./...`

- `fizzbuzz` package are located in `fizzbuzz/fizzbuzz_test.go`
- `api` package at `api/api_test.go` `` and


## Endpoint `/fizzbuzz`

Note: If not provided, default parameters are `limit = 100`, `str1 = "fizz"`, `str2 = "buzz"`

### Examples


`GET http://localhost:8080/fizzbuzz?int1=3&int2=5&limit=16&str1=fizz&str2=buzz` (Original fizzbuzz)

```json
[
  "1",
  "2",
  "fizz",
  "4",
  "buzz",
  "fizz",
  "7",
  "8",
  "fizz",
  "buzz",
  "11",
  "fizz",
  "13",
  "14",
  "fizzbuzz",
  "16"
]
```

`GET http://localhost:8080/fizzbuzz?int1=2&int2=3` (Default parameters `limit`, `str1`, `str2`)
```json
[
  "1",
  "fizz",
  "buzz",
  "fizz",
  "5",
  "fizzbuzz",
  
  "..."
]
```

`GET http://localhost:8080/fizzbuzz?int1=nothing` (Unexpected value)

```json
{
   "error": "strconv.Atoi: parsing \"nothing\": invalid syntax",
   "message": "int1 param should be an non zero integer"
}
```

## Endpoint `/stats`
Returns a dictionary with a "most_requested" entry, with an object's:
  - key: a ", " separated string corresponding to the parameters of the most frequently used request
  - value: the number of hits for this request

### Example

`GET /stats`

```json
{
  "most_requested": {
    "3, 5, 100, fizz, buzz": 199
  }
}
```

Note: if no request on /fizzbuzz endpoint have been performed, an empty object will be returned
```json
{
  "most_requested": {}
}
```
