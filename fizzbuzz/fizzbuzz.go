package fizzbuzz

import (
	"strconv"
	"strings"
)

// Fizzbuzz This implementation does less test that
func Fizzbuzz(int1 int, int2 int, limit int, str1 string, str2 string) string {
	var res []string

	// Swapping int1 and int2, str1 and str2 to respect the precedence implied by the assignment
	if int1 > int2 {
		// A third value is not necessary here as the compiler handle variables swaps perfectly. See https://stackoverflow.com/questions/35707222/swap-two-numbers-golang
		int1, int2 = int2, int1
		str1, str2 = str2, str1
	}
	entry := ""
	for i := 1; i <= limit; i++ {
		if i%int1 == 0 {
			entry += str1
		}
		if i%int2 == 0 {
			entry += str2
		}
		if entry == "" {
			entry = strconv.Itoa(i)
		}
		res = append(res, entry)
		entry = ""
	}
	return strings.Join(res, ",")
}
