package fizzbuzz

import (
	"testing"
)

// TestFizzBuzzExample1 calls main.fizzbuzz with values (3, 5, 16, "fizz", "buzz"),
// checking for a difference in comparison of the given example in the assignment.
func TestFizzBuzzExample1(t *testing.T) {
	const limit = 16

	expectedVal := "1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,fizzbuzz,16"
	msg := Fizzbuzz(3, 5, limit, "fizz", "buzz")

	if expectedVal != msg {
		t.Fatalf("fizzbuz =\n %q, want\n %q, error", msg, expectedVal)
	}
}

// TestFizzBuzzArgsSwap calls main.fizzbuzz with values (2, 4,...) and (4, 2,...),
// It is implied by the given example (in the assignment) that the highest divider has a priority over the lower ones.
// Hence, we should make sure that priority rule is consistent whatever the order of the given parameters.
func TestFizzBuzzArgsSwap(t *testing.T) {
	msg1 := Fizzbuzz(2, 4, 100, "fizz", "buzz")
	msg2 := Fizzbuzz(4, 2, 100, "buzz", "fizz")

	if msg1 != msg2 {
		t.Fatalf("fizzbuz =\n %q, want\n %q, error", msg1, msg2)
	}
}
func TestFizzBuzzArgsSwap2(t *testing.T) {
	msg1 := Fizzbuzz(05, -2, 100, "fizz", "buzz")
	msg2 := Fizzbuzz(-2, 05, 100, "buzz", "fizz")

	if msg1 != msg2 {
		t.Fatalf("fizzbuz =\n %q, want\n %q, error", msg1, msg2)
	}
}

// TestFizzBuzzDefaultArgs calls main.fizzbuzz with limit value <= 0
func TestFizzBuzzDefaultArgs(t *testing.T) {
	if res := Fizzbuzz(2, 3, 0, "", ""); res != "" {
		t.Fatalf("fizzbuz with limit 0 return %q want ''", res)
	}
	if res := Fizzbuzz(2, 3, -1, "", ""); res != "" {
		t.Fatalf("fizzbuz with limit <0 return %q want ''", res)
	}
}
